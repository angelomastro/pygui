# already contains devtools, python interpreter, bash etc
FROM ubuntu:16.04 

# utilities 
RUN apt-get update \
    && apt-get install -y \
       build-essential \
       software-properties-common \
       wget \
       sudo \
       vim \
       nano \
       gdb \
       tree \
       valgrind \
    && apt-get clean && apt-get autoremove -y && rm -rf /var/lib/apt/lists/* 

# ubuntu pre-requisites
### libgtk is A MUST
### wx.grid requires libsdl1.2-dev
RUN apt-get update \
    && apt install -y libwxgtk3.0-dev libsdl1.2-dev


# Python and libs
### notes: matplotlib 2.2.2 works for python2.7, otherwise it's python3.5
RUN apt-get update && apt-get install python python-pip -y \
    && pip install \
       -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 \
       wxPython

# create a user of the docker image
RUN useradd --create-home --uid 1000 --groups sudo --shell /bin/bash developer \
    && mkdir -p /etc/sudoers.d \
    && echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer \
    && chmod 0440 /etc/sudoers.d/developer
ENV HOME /home/developer
USER developer


# special files and env var
## python autocomplete 
RUN touch /home/developer/.pythonrc \
    && echo "import rlcompleter, readline" >>  /home/developer/.pythonrc \
    && echo "readline.parse_and_bind('tab:complete')" >>  /home/developer/.pythonrc \
    && echo "export PYTHONSTARTUP=\"/home/developer/.pythonrc\"" >> /home/developer/.bashrc


RUN echo 'alias c="clear"' >> ~/.bashrc
RUN echo 'alias x="exit"' >> ~/.bashrc

WORKDIR /home/developer/workspace
