#! /bin/bash

docker run -it --rm \
          --env=XAUTHORITY= \
          --env=DISPLAY=:0 \
          --privileged \
          --net host \
          -v $(pwd):/home/developer/workspace \
          pygui:dev bash
