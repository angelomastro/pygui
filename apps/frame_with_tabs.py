#! /usr/bin/python

import wx
import wx.aui

class MainFrame(wx.aui.AuiMDIParentFrame):

    def __init__(self, *args, **kw):
        super(MainFrame, self).__init__(*args, **kw)
        
        tab1 = MainTab(self, title="Main", size=wx.Size(400,400))
        self.SetActiveChild(tab1)
    
        self.CreateStatusBar()
        self.SetStatusText("Ready")
    
        

class MainTab(wx.aui.AuiMDIChildFrame):

    def __init__(self, *args, **kw):
        super(MainTab, self).__init__(*args, **kw)

        self.panel = wx.Panel(self, size=wx.Size(400,400))

        self.fp1 = wx.FilePickerCtrl(self.panel, 
                        wildcard="Input file", 
                        pos=wx.Point(10,10),
                        size=wx.Size(300,30))
        
        self.fp2 = wx.FilePickerCtrl(self.panel, 
                        wildcard="Output file", 
                        pos=wx.Point(10,60),
                        size=wx.Size(300,30))
        
        self.bt1 = wx.Button(self.panel, 
                        label="Import", 
                        pos=wx.Point(10,110),
                        size=wx.Size(100,30))



if __name__ == '__main__':

    app = wx.App()
    frm = MainFrame(None, title='File converter app')
    frm.Show()
    app.MainLoop()
