### Python GUI


Using [wxPython](https://wxpython.org/pages/overview/). Requires [docker ce](https://docs.docker.com/install/linux/docker-ce/ubuntu/). 

The main source of inspiration to add new widgets on a desktop app is [this](https://docs.wxpython.org/gallery.html). 

### Install

Build a virtual environment with `./build.sh`, go inside of it with `./start.sh`. 

### Quick test

This will show a gui from the dev environment. In case all got installed correctly 

```
python -c "import wx; a=wx.App(); wx.Frame(None, title=\"Hello World\").Show(); a.MainLoop()"
```

### My drafts

Example guis are in the **apps** folder. For example

`./apps/helloworld2.py`


Have fun trying your own GUI! 
